import 'package:toast/toast.dart';
import 'package:flutter/material.dart';


void showToast(String msg,context, int duration, int gravity, Color backgroundColor) {
    Toast.show(msg, context, duration: duration, gravity: gravity,backgroundColor:backgroundColor);
  }