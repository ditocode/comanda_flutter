import 'dart:io';

import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;

Future<bool> getCurrentUser(BuildContext context) async {
  try {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    //Path si no cuenta con sesión
    Navigator.of(context).pushNamed('/Login');
    return false;
  }
}

Future<FirebaseUser> getuser() async {
  try {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user != null) {
      return user;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

void signOut(BuildContext context, OrderModel model) async {
  try {
    FirebaseAuth.instance.signOut().then((onValue) {
      model.setOrderId("");
      Navigator.of(context).pushReplacementNamed('/Login');
    });
  } catch (e) {
    //Mostrar toast de error
  }
}

Future<dynamic> requestUser(type, url, body, context) async {
  try {
    var contextUrl =
        "https://us-central1-restaurantes-e2bc7.cloudfunctions.net" + url;
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    var headers = {HttpHeaders.authorizationHeader: ''};

    if (user != null) {
      String token = await user.getIdToken(refresh: true);
      print(token);
      headers = {HttpHeaders.authorizationHeader: token};
    }

    var response;

    //Va por el Token del usuario
    if (type == "POST") {
      response = await http.post(contextUrl, body: body, headers: headers);
      return response;
    } else if (type == "GET") {
      response = await http.get(contextUrl, headers: headers);
      return response;
    } else {
      return response;
    }
  } catch (e) {
    print(e);
  }
}
