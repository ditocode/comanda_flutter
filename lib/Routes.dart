import 'dart:async';

import 'package:comandapp/screens/Account/index.dart';
import 'package:comandapp/screens/EditInfo/index.dart';
import 'package:comandapp/screens/Record/RecordDetail.dart';
import 'package:comandapp/screens/Record/index.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/screens/Filter/index.dart';
import 'package:comandapp/screens/IndianFoodMenu/index.dart';
import 'package:comandapp/screens/PhoneNumber/index.dart';
import 'package:comandapp/screens/WesternFoodMenu/index.dart';
import 'package:comandapp/screens/splashScreen/index.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/screens/Location/index.dart';
import 'package:comandapp/screens/QRViewer/QRScan.dart';
import 'package:comandapp/screens/Login/index.dart';
import 'package:comandapp/screens/Favourites/index.dart';
import 'package:comandapp/screens/HomeWithTab/index.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:comandapp/screens/SingUp/index.dart';

class Routes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ScopedModel<OrderModel>(
      model: OrderModel(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        color: Colors.red,
        home: _getLandingPage(),
        routes: routes,
        theme: appTheme,
      ),
    );
  }

  var routes = <String, WidgetBuilder>{
    '/Login'            : (BuildContext context) => new Login(),
    '/SingUp'           : (BuildContext context) => new SingUp(),
    '/ScanQR'           : (BuildContext context) => new QRScan(),
    '/Favourites'       : (BuildContext context) => const Favourites(),
    '/HomeWithTab'      : (BuildContext context) => new HomeWithTab(),
    '/PhoneNumber'      : (BuildContext context) => new PhoneNumber(),
    '/IndianFoodMenu'   : (BuildContext context) => const IndianFoodMenu(),
    '/WesternFoodMenu'  : (BuildContext context) => const WesternFoodMenu(),
    '/Filter'           : (BuildContext context) => new Filter(),
    '/splashScreen'     : (BuildContext context) => const SplashScreen(),
    '/EditInfo'         : (BuildContext context) => new EditInfo(),
    '/Account'          : (BuildContext context) => new Account(),
    '/Record'           : (BuildContext context) => new Record(),
    '/RecordDetail'     : (BuildContext context) => new RecordDetail()
  };


  Widget _getLandingPage() {
    return StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          print("LOGIN");
          if (snapshot.data.providerData.length == 1) {
            // logged in using email and password
            return SplashScreen();
          } else {
            // logged in using other providers
            return SplashScreen();
          }
        } else {
          print("NO LOGIN");
          return Login();
        }
      },
    );
  }
}
