import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/foundation/platform.dart';
import 'package:comandapp/theme/style.dart';

class PhoneNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.chevron_left,
              size: 40.0,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: new Text(
          "Login",
          style: textStylew500,
        ),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: new Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Text(
                "Please enter your phone number",
                textAlign: TextAlign.center,
                softWrap: true,
                style: const TextStyle(
                  color: const Color.fromRGBO(153, 153, 153, 1.0),
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
              new Container(
                decoration: new BoxDecoration(
                    border: new Border(
                  bottom: new BorderSide(
                    color: Colors.grey[400],
                  ),
                )),
                margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                child: new Row(
                  children: <Widget>[
                    new Container(
                      alignment: Alignment.centerLeft,
                      width: 50.0,
                      child: new Text(
                        "+91-",
                        style: new TextStyle(
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                    new Expanded(
                      child: new Container(
                        child: new TextField(
                          decoration:
                              new InputDecoration(border: InputBorder.none),
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              new Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: new InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacementNamed("/HomeWithTab");
                  },
                  child: new Container(
                    child: new Text(
                      defaultTargetPlatform == TargetPlatform.iOS
                          ? "Proceed"
                          : "PROCEED",
                      style:
                          const TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                    width: screenSize.width - 80,
                    height: 45.0,
//                margin: new EdgeInsets.only(
//                    top: 20.0, bottom: 20.0, left: 10.0, right: 10.0),
                    alignment: FractionalOffset.center,
                    decoration: new BoxDecoration(
                      color: new Color.fromRGBO(33, 127, 255, 20.0),
                      borderRadius:
                          const BorderRadius.all(const Radius.circular(5.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
