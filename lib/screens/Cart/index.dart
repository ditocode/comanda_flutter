import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/screens/Cart/data.dart';
import 'package:comandapp/screens/HomeWithTab/index.dart';
import 'package:comandapp/theme/style.dart';

FocusNode _myNode = new FocusNode()..addListener(listener);
ScrollController scrollController = new ScrollController(
  initialScrollOffset: 0.0,
  keepScrollOffset: true,
);
Size screenSize;

class Cart extends StatelessWidget {
  final DataListBuilder orderList = new DataListBuilder();

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return new Scaffold(
//      resizeToAvoidBottomPadding: false,
//      primary: false,

      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.chevron_left,
              size: 40.0,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed('/HomeWithTab');
            }),
        centerTitle: true,
        title: new Text(
          'Review Order',
          style: textStylew500,
        ),
        elevation: 0.0,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: new ListView(
        controller: scrollController,
        children: <Widget>[
          new Container(
            child: new Padding(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: const ExactAssetImage(
                            'assets/home-page/rajdhani.jpg'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: new BorderRadius.circular(5.0),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Rajdhani',
                          style: Theme.of(context).primaryTextTheme.display1,
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: new Text(
                            'BTM 2nd Stage',
                            style: defaultTargetPlatform == TargetPlatform.iOS
                                ? Theme.of(context).primaryTextTheme.display2
                                : Theme.of(context).primaryTextTheme.display4,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              padding: const EdgeInsets.all(10.0),
            ),
            color: Colors.white,
            margin: const EdgeInsets.only(bottom: 5.0),
          ),
          new CustomCardReviewOrder(
            itemList: orderList.itemList,
          ),
          new Container(
            height: 50.0,
            margin: const EdgeInsets.only(top: 5.0, bottom: 1.0),
            padding: const EdgeInsets.only(top: 15.0, bottom: 10.0, left: 10.0),
            color: Colors.white,
            child: new Text(
              'DETAILED BILL',
              style: textStyle12Bold,
            ),
          ),
          new Column(
            children: <Widget>[
              new Container(
                color: Colors.white,
                padding: new EdgeInsets.all(10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'Item Total(${orderList.itemList.length})',
                      style: const TextStyle(
                          fontSize: 15.0,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                    ),
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          const Text(
                            'Rs. 880',
                            style: const TextStyle(
                                color:
                                    const Color.fromRGBO(153, 153, 153, 1.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                color: Colors.white,
                padding: new EdgeInsets.all(10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'Packaging Charges',
                      style: const TextStyle(
                          fontSize: 15.0,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                    ),
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          const Text(
                            'Rs. 20',
                            style: const TextStyle(
                                color:
                                    const Color.fromRGBO(153, 153, 153, 1.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                color: Colors.white,
                padding: new EdgeInsets.all(10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'GST',
                      style: const TextStyle(
                          fontSize: 15.0,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                    ),
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          const Text(
                            'Rs. 200',
                            style: const TextStyle(
                                color:
                                    const Color.fromRGBO(153, 153, 153, 1.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                color: Colors.white,
                padding: new EdgeInsets.all(10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'Delivery Charges',
                      style: const TextStyle(
                          fontSize: 15.0,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                    ),
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          const Text(
                            'Rs. 20',
                            style: const TextStyle(
                                color:
                                    const Color.fromRGBO(153, 153, 153, 1.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                color: Colors.white,
                padding: new EdgeInsets.all(10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'Grand Total',
                      style: const TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          const Text(
                            'Rs. 1120',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          new Container(
            height: 50.0,
            margin: const EdgeInsets.only(top: 5.0, bottom: 1.0),
            padding: const EdgeInsets.only(top: 15.0, bottom: 10.0, left: 10.0),
            color: Colors.white,
            child: new Text(
              'DETAILS',
              style: textStyle12Bold,
            ),
          ),
          new Container(
            padding: const EdgeInsets.only(left: 20.0),
            decoration: new BoxDecoration(
              color: Colors.white,
              border: new Border(
                bottom: new BorderSide(
//                  width: 0.5,
                  color: Colors.black26,
                ),
              ),
            ),
            child: new Row(
              children: <Widget>[
                new Container(
                  width: 100.0,
                  child: new Text(
                    'Email',
                    style: new TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
                new Expanded(
                  child: new TextField(
                    decoration: new InputDecoration(border: InputBorder.none),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: const EdgeInsets.only(left: 20.0),
            decoration: new BoxDecoration(
              color: Colors.white,
              border: new Border(
                bottom: new BorderSide(
                  width: 0.5,
                  color: Colors.grey,
                ),
              ),
            ),
            child: new Row(
              children: <Widget>[
                new Container(
                  width: 100.0,
                  child: new Text(
                    'Phone',
                    style: new TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
                new Expanded(
                  child: new TextField(
                    decoration: new InputDecoration(border: InputBorder.none),
                    keyboardType: TextInputType.phone,
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: const EdgeInsets.only(
                left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
            color: Colors.white,
            child: new Container(
                padding: const EdgeInsets.only(left: 10.0),
                decoration: new BoxDecoration(
                  color: Colors.grey[300],
                ),
                child: new TextFormField(
                  focusNode: _myNode,
                  maxLines: 6,
                  decoration: new InputDecoration(
                      border: InputBorder.none, hintText: 'Enter Address'),
                )),
          ),
          new Container(
            color: Colors.white,
            padding: const EdgeInsets.only(bottom: 10.0),
            child: new FlatButton(
              onPressed: () {
                showDialog<Null>(
                  context: context,
                  barrierDismissible: true, // user must tap button!
                  builder: (BuildContext context) {
                    return new AlertDialog(
                      title: new Text('Order placed'),
//                      content: new SingleChildScrollView(
//                        child: new ListBody(
//                          children: <Widget>[
//                            new Text('You will never be satisfied.'),
//                            new Text('You\’re like me. I’m never satisfied.'),
//                          ],
//                        ),
//                      ),
                      actions: <Widget>[
                        new FlatButton(
                          child: new Text('Okay'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
              child: new Container(
                child: new Text(
                  defaultTargetPlatform == TargetPlatform.android
                      ? 'CONFIRM'
                      : 'Confirm',
                  style: const TextStyle(color: Colors.white, fontSize: 14.0),
                ),
                width: screenSize.width - 20,
                height: 45.0,
                alignment: FractionalOffset.center,
                decoration: const BoxDecoration(
                  color: const Color.fromRGBO(33, 127, 255, 20.0),
                  borderRadius:
                      const BorderRadius.all(const Radius.circular(5.0)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomCardReviewOrder extends StatefulWidget {
  final List<CartItems> itemList;

  CustomCardReviewOrder({this.itemList});

  @override
  CustomCardReviewOrderState createState() =>
      new CustomCardReviewOrderState(itemList: itemList);
}

class CustomCardReviewOrderState extends State<CustomCardReviewOrder> {
  final List<CartItems> itemList;

  CustomCardReviewOrderState({this.itemList});

  @override
  Widget build(BuildContext context) {
    return new Column(
        children: itemList.map((CartItems itemData) {
      return new Container(
        color: Colors.white,
        child: new ListTile(
          title: new Text(
            itemData.name,
            style: const TextStyle(fontSize: 15.0),
          ),
          trailing: new Container(
            width: 165.0,
            child: new Row(
              children: <Widget>[
                new IconButton(
                    icon: new Icon(
                      Icons.remove_circle_outline,
                      color: Theme.of(context).primaryColor,
                      size: 18.0,
                    ),
                    onPressed: () {
                      setState(() {
                        if (itemData.quantity > 0) {
                          itemData.quantity--;
                        }
                      });
                    }),
                new Text(
                  '0' + itemData.quantity.toString(),
                  style: defaultTargetPlatform == TargetPlatform.iOS
                      ? new TextStyle(
                          fontWeight: FontWeight.w100,
                          color: const Color.fromRGBO(153, 153, 153, 1.0))
                      : new TextStyle(
                          fontWeight: FontWeight.w400,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                ),
                new IconButton(
                    icon: new Icon(
                      Icons.add_circle_outline,
                      color: Theme.of(context).primaryColor,
                      size: 18.0,
                    ),
                    onPressed: () {
                      setState(() {
                        itemData.quantity++;
                      });
                    }),
                new Text(
                  'Rs. ' + itemData.amount.toString(),
                  style: defaultTargetPlatform == TargetPlatform.iOS
                      ? new TextStyle(
                          fontWeight: FontWeight.w100,
                          color: const Color.fromRGBO(153, 153, 153, 1.0))
                      : new TextStyle(
                          fontWeight: FontWeight.w400,
                          color: const Color.fromRGBO(153, 153, 153, 1.0)),
                ),
              ],
            ),
          ),
        ),
      );
    }).toList());
  }
}

listener() {
  if (_myNode.hasFocus) {
    print(1);
    // keyboard appeared
    scrollController.animateTo(
      1000.0,
//      1 * screenSize.height / 2,
      curve: Curves.linear,
      duration: const Duration(milliseconds: 400),
    );
  } else {
    // keyboard dismissed
  }
}
