import 'package:flutter/material.dart';

class NoDishes extends StatelessWidget {
  String msg = "";

  NoDishes({this.msg});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.mood_bad,
              size: 50.0,
              color: Colors.yellow[700],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Text( msg != "" ? msg :'No cuentas con platillos seleccionados.',
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
