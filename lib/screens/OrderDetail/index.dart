import 'dart:async';
import 'dart:convert';

import 'package:comandapp/components/cardMenuData.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:comandapp/screens/DishDetail/index.dart';
import 'package:comandapp/tools/tool.dart';
import 'package:comandapp/tools/user.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

FocusNode _myNode = new FocusNode()..addListener(listener);
ScrollController scrollController = new ScrollController(
  initialScrollOffset: 0.0,
  keepScrollOffset: true,
);
Size screenSize;

class OrderDetail extends StatelessWidget {
  TabController tabController;

  OrderDetail({this.tabController});

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return new Scaffold(
//      resizeToAvoidBottomPadding: false,
//      primary: false,

      appBar: new AppBar(
        centerTitle: true,
        title: new Text(
          'Orden',
          style: textStylew500,
        ),
        elevation: 0.0,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: ScopedModelDescendant<OrderModel>(
          builder: (context, child, model) {
            if (model.items.length == 0) {
              return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.mood_bad,
                        size: 50.0,
                        color: Colors.yellow[700],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Text(
                          'No cuentas con platillos seleccionados.',
                          style: TextStyle(
                              fontSize: 14.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
              );
            } else {
              return new ListView(
                controller: scrollController,
                children: <Widget>[
                  new CustomCardReviewOrder(
                    itemList: model.items,
                  ),
                  new Container(
                    height: 50.0,
                    margin: const EdgeInsets.only(top: 5.0, bottom: 1.0),
                    padding: const EdgeInsets.only(
                        top: 15.0, bottom: 10.0, left: 10.0),
                    color: Colors.white,
                    child: new Text(
                      'COSTO DE ESTA ORDEN',
                      style: textStyle12Bold,
                    ),
                  ),
                  new Column(
                    children: <Widget>[
                      new Container(
                        color: Colors.white,
                        padding: new EdgeInsets.all(10.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text(
                              'Total Platillos(${model.items.length})',
                              style: const TextStyle(
                                  fontSize: 15.0,
                                  color:
                                      const Color.fromRGBO(153, 153, 153, 1.0)),
                            ),
                            new Container(
                              child: new Row(
                                children: <Widget>[
                                  const Text(
                                    '',
                                    style: const TextStyle(
                                        color: const Color.fromRGBO(
                                            153, 153, 153, 1.0)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.white,
                        padding: new EdgeInsets.all(10.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text(
                              'Total (Sin Propina)',
                              style: const TextStyle(
                                fontSize: 15.0,
                              ),
                            ),
                            new Container(
                              child: new Row(
                                children: <Widget>[
                                  new Text(
                                    model.getTotal(),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    color: Colors.white,
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: new FlatButton(
                      onPressed: () {
                        showDialog<Null>(
                          context: context,
                          barrierDismissible: true, // user must tap button!
                          builder: (BuildContext context) {
                            return new AlertDialog(
                              title: new Text('Confirmar Orden'),
                              content: Text('¿Está seguro de ordenar?'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                new FlatButton(
                                  child: new Text('Sí'),
                                  onPressed: () {
                                    if (model.items.length > 0) {
                                      print(model.items);
                                      var items = model.items
                                          .map((e) => e.objToSend())
                                          .toList();

                                      var tableJson = model.table.toJsonAttr();

                                      var dataToSend = {
                                        'items': json.encode(items),
                                        'idSucursal': model.sucursal,
                                        'table': json.encode(tableJson)
                                      };

                                      if (model.orderId != "") {
                                        dataToSend['idOrder'] = model.orderId;
                                      }

                                      requestUser("POST", '/orders/addOrder',
                                              dataToSend, context)
                                          .then((response) {
                                        var resultRequest =
                                            json.decode(response.body);

                                        if (response.statusCode != 200) {
                                        } else {
                                          var orderId =
                                              resultRequest['idOrder'];
                                          model.setOrderId(orderId);
                                          model.cleanItems(
                                              context, tabController);

                                          showToast(
                                              resultRequest['msg'],
                                              context,
                                              Toast.LENGTH_LONG,
                                              Toast.BOTTOM,
                                              Colors.green[400]);
                                        }
                                      }).catchError((onError) {
                                        print(onError);
                                      });
                                    } else {
                                      showToast(
                                          "Aún no cuenta con platillos a ordenar",
                                          context,
                                          Toast.LENGTH_LONG,
                                          Toast.BOTTOM,
                                          Colors.red[400]);
                                    }
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: new Container(
                        child: new Text(
                          defaultTargetPlatform == TargetPlatform.android
                              ? 'CONFIRMAR'
                              : 'Confirmar',
                          style: const TextStyle(
                              color: Colors.white, fontSize: 14.0),
                        ),
                        width: screenSize.width - 20,
                        height: 45.0,
                        alignment: FractionalOffset.center,
                        decoration: const BoxDecoration(
                          color: const Color.fromRGBO(33, 127, 255, 20.0),
                          borderRadius: const BorderRadius.all(
                              const Radius.circular(5.0)),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}

class CustomCardReviewOrder extends StatefulWidget {
  final List<ItemOrder> itemList;

  CustomCardReviewOrder({this.itemList});

  @override
  CustomCardReviewOrderState createState() =>
      new CustomCardReviewOrderState(itemList: itemList);
}

class CustomCardReviewOrderState extends State<CustomCardReviewOrder> {
  final List<ItemOrder> itemList;

  CustomCardReviewOrderState({this.itemList});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScopedModelDescendant<OrderModel>(
        builder: (context, child, model) => new Column(
            children: itemList
                .asMap()
                .map((i, ItemOrder itemData) => MapEntry(
                      i,
                      new Container(
                        margin: const EdgeInsets.only(bottom: 1.0),
                        child: new Dismissible(
                          key: Key(UniqueKey().toString()),
                          onDismissed: (DismissDirection direction) {
                            if (direction == DismissDirection.endToStart) {
                              var route = new MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    new DishDetail(
                                  dish: itemData,
                                  indexToEdit: i,
                                ),
                              );

                              Navigator.of(context).push(route);
                            } else {
                              model.deleteItem(itemData);
                            }
                          },
                          background: Container(
                            color: Colors.red,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    "Eliminar",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          secondaryBackground: Container(
                            color: Colors.green,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 10.0, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Editar",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          child: Container(
                              color: Colors.white,
                              child: new Column(
                                children: <Widget>[
                                  new ListTile(
                                    title: new Text(
                                      itemData.currentName,
                                      style: const TextStyle(fontSize: 15.0),
                                    ),
                                    trailing: new Container(
                                      width: 165.0,
                                      child: new Row(
                                        children: <Widget>[
                                          new IconButton(
                                              icon: new Icon(
                                                Icons.remove_circle_outline,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                size: 18.0,
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  if (itemData.quantity > 0) {
                                                    itemData.quantity--;
                                                    if (itemData.quantity ==
                                                        0) {
                                                      model
                                                          .deleteItem(itemData);
                                                    } else {
                                                      model.updateItem(
                                                          i, itemData);
                                                    }
                                                  }
                                                });
                                              }),
                                          new Text(
                                            '0' + itemData.quantity.toString(),
                                            style: defaultTargetPlatform ==
                                                    TargetPlatform.iOS
                                                ? new TextStyle(
                                                    fontWeight: FontWeight.w100,
                                                    color: const Color.fromRGBO(
                                                        153, 153, 153, 1.0))
                                                : new TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    color: const Color.fromRGBO(
                                                        153, 153, 153, 1.0)),
                                          ),
                                          new IconButton(
                                              icon: new Icon(
                                                Icons.add_circle_outline,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                size: 18.0,
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  itemData.quantity++;
                                                  model.updateItem(i, itemData);
                                                });
                                              }),
                                          new Text(
                                            '\$' +
                                                itemData.price
                                                    .toStringAsFixed(2),
                                            style: defaultTargetPlatform ==
                                                    TargetPlatform.iOS
                                                ? new TextStyle(
                                                    fontWeight: FontWeight.w100,
                                                    color: const Color.fromRGBO(
                                                        153, 153, 153, 1.0))
                                                : new TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    color: const Color.fromRGBO(
                                                        153, 153, 153, 1.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  itemData.selectedComplements.length > 0
                                      ? new ExpansionTile(
                                          title: Text("Complementos"),
                                          children: itemData.selectedComplements
                                              .map(buildComplementoOption)
                                              .toList(),
                                        )
                                      : new Text("")
                                ],
                              )),
                        ),
                      ),
                    ))
                .values
                .toList()),
      ),
    );
  }
}

Widget buildComplementoOption(ComplementoOption option) {
  return new ListTile(
    title: new Text(
      option.nombre,
      style: const TextStyle(fontSize: 15.0),
    ),
    trailing: new Container(
      alignment: Alignment.centerRight,
      width: 60.0,
      child: new Text('\$${option.precio.toStringAsFixed(2)}',
          style: defaultTargetPlatform == TargetPlatform.iOS
              ? new TextStyle(
                  fontWeight: FontWeight.w100,
                  color: const Color.fromRGBO(153, 153, 153, 1.0))
              : new TextStyle(
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(153, 153, 153, 1.0))),
    ),
  );
}

listener() {
  if (_myNode.hasFocus) {
    print(1);
    // keyboard appeared
    scrollController.animateTo(
      1000.0,
//      1 * screenSize.height / 2,
      curve: Curves.linear,
      duration: const Duration(milliseconds: 400),
    );
  } else {
    // keyboard dismissed
  }
}
