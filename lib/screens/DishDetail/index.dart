import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/components/cardMenuData.dart';
import 'package:comandapp/theme/style.dart';
import 'package:scoped_model/scoped_model.dart';

class DishDetail extends StatefulWidget {
  final ItemOrder dish;
  final int indexToEdit;

  const DishDetail({this.dish, this.indexToEdit});
  @override
  _DishDetailState createState() => _DishDetailState();
}

class _DishDetailState extends State<DishDetail> {
  final notesController = TextEditingController();
  int items = 1;
  double price = 0;
  double total = 0;
  bool isEditing = false;
  List<ComplementoOption> _complementos = <ComplementoOption>[];
  //List<ComplementoOption> _complementosTemp = <ComplementoOption>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      price = widget.dish.price;
      if (widget.dish.quantity != null) {
        items = widget.dish.quantity;
        _complementos = widget.dish.selectedComplements;
        isEditing = true;
      } else {
        total = price;
      }

      if (widget.dish.instructions != "") {
        notesController.text = widget.dish.instructions;
      } else {
        notesController.text = "";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    getTotal();

    final Size screenSize = MediaQuery.of(context).size;
    return ScopedModelDescendant<OrderModel>(
      builder: (context, child, model) => new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.dish.currentName, style: textStylew500),
          centerTitle: true,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        body: new ListView(
          children: <Widget>[
            new SizedBox(
              height: screenSize.height / 4,
              child: new Stack(
                children: <Widget>[
                  new Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new Image.network(widget.dish.thumbail).image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  new Container(
                    height: screenSize.height / 4,
                    width: screenSize.width,
                    decoration: new BoxDecoration(
                      color: Colors.black54,
                    ),
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            '\$${price.toStringAsFixed(2)} MXN',
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ]),
                  ),
                ],
              ),
            ),
            Card(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.dish.description != null
                          ? widget.dish.description
                          : "",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ),
            ),
            new Column(
              children: widget.dish.complements.map(buildComplementos).toList(),
            ),
            new ExpansionTile(
              initiallyExpanded: true,
              title: new Text(
                "Instrucciones.",
                style: new TextStyle(fontSize: 12.0),
              ),
              children: <Widget>[
                new Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                    color: Colors.white,
                    child: TextField(
                      controller: notesController,
                      maxLength: 100,
                      decoration: InputDecoration(
                        hintText: 'Notas (Sin crema, Sin picante, etc..',
                      ),
                    ))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.remove_circle_outline),
                  onPressed: () {
                    setState(() {
                      if (items > 1) {
                        items--;
                      }
                    });
                  },
                ),
                Text(
                  '${items}',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0),
                ),
                IconButton(
                  icon: Icon(Icons.add_circle_outline),
                  onPressed: () {
                    setState(() {
                      items++;
                    });
                  },
                ),
              ],
            )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
            child: new GestureDetector(
                onTap: () {
                  ItemOrder item = new ItemOrder(
                      price: price,
                      quantity: items,
                      thumbail: widget.dish.thumbail,
                      currentName: widget.dish.currentName,
                      id: widget.dish.id,
                      instructions: notesController.text,
                      phase: 0,
                      complements: widget.dish.complements,
                      selectedComplements: _complementos,
                      description: widget.dish.description
                      );
                  if (isEditing) {
                    model.updateItemNav(widget.indexToEdit, item, context);
                  } else {
                    model.addItem(item, context);
                  }
                },
                child: BottomAppBar(
                  color: Color.fromRGBO(7, 189, 103, 1),
                  child: new Row(
                    textDirection: TextDirection.rtl,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                            'Agregar a la orden ${items} - \$${total.toStringAsFixed(2)} MXN',
                            style: new TextStyle(
                                color: Color.fromRGBO(254, 254, 254, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0)),
                      )
                    ],
                  ),
                ))),
      ),
    );
  }
  int maximo = 0;
  int contador = 0;

  Widget buildComplementos(Complemento complemento) {
    var titulo = complemento.question;
    if(complemento.max != 0){
      titulo += " (Selección maxima ${complemento.max})";
    }
    if (!complemento.optional) {
      titulo += " (Obligatorio)";
    }
    
    maximo += complemento.max;
    //print(complemento.max);

    return new ExpansionTile(
      onExpansionChanged: (v) {
        if(v){
          contador = 0;
        }
      },
      title: Text(
        titulo,
        style: new TextStyle(fontSize: 12.0),
      ),
      children: <Widget>[
        Column(
          children: complemento.options.map((ComplementoOption option) {
            var selected = false;

            if (_complementos.indexOf(option) != -1) {
              selected = true;
            }

            if (option.agotado != null && option.agotado) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Checkbox(
                      value: false,
                      onChanged: null,
                    ),
                  ),
                  Expanded(
                    child: Text(option.nombre),
                  ),
                  Expanded(
                    child: Text(
                      "Agotado",
                      style: new TextStyle(color: Colors.red[400]),
                    ),
                  )
                ],
              );
            } else {
              return  Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Checkbox(
                      value: selected,
                      onChanged: (bool value) {
                        if(maximo == 0){
                          setState(() {
                            if(value){
                              _complementos.add(option);
                              total += option.precio;
                            }else{
                              _complementos.remove(option);
                              total -= option.precio;
                            }
                          });
                        }else{
                          if((_complementos.length < maximo) && (contador < complemento.max)){
                            setState(() {
                              if(value){
                                _complementos.add(option);
                                contador++;
                                total += option.precio;
                              }else{
                                _complementos.remove(option);
                                contador--;
                                total -= option.precio;
                              }
                            });
                          }else{
                            setState(() {
                              if(!value){
                                _complementos.remove(option);
                                contador--;
                                total -= option.precio;
                              }
                            });
                          }
                        }
                      }
                    ),
                  ),
                  Expanded(
                    child: Text(option.nombre),
                  ),
                  Expanded(
                    child: Text('+\$${option.precio.toStringAsFixed(2)}'),
                  )
                ],
              );
            }
          }).toList(),
        )
      ],
    );
  }

  Widget buildOptionsComplemento(ComplementoOption option) {
    var selected = false;

    if (_complementos.indexOf(option) != -1) {
      selected = true;
      //print(max);
    }

    if (option.agotado != null && option.agotado) {
      return new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Checkbox(
              value: false,
              onChanged: null,
            ),
          ),
          Expanded(
            child: Text(option.nombre),
          ),
          Expanded(
            child: Text(
              "Agotado",
              style: new TextStyle(color: Colors.red[400]),
            ),
          )
        ],
      );
    } else {
      return new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Checkbox(
              value: selected,
              onChanged: (value) {
                if (value) {
                  setState(() {
                    _complementos.add(option);
                  });
                } else {
                  setState(() {
                    _complementos.remove(option);
                    total -= option.precio;
                  });
                }
              },
            ),
          ),
          Expanded(
            child: Text(option.nombre),
          ),
          Expanded(
            child: Text('+\$${option.precio.toStringAsFixed(2)}'),
          )
        ],
      );
    }
  }

  getTotal() {
    
    double totalComplementos = 0.0;
    _complementos.forEach((complemento) {
      totalComplementos += complemento.precio;
    });

    var totalItems = (price+totalComplementos) * items;

    setState(() {
      total = totalItems;
    });

    maximo = 0;
  }
}
