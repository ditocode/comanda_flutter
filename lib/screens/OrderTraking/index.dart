import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:comandapp/screens/OrderDetail/index.dart';
import 'package:comandapp/screens/OrderTraking/Tracking.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/tools/user.dart';
import 'package:comandapp/screens/Commons/loading.dart' as Loader;
import 'package:comandapp/screens/Commons/noDishes.dart' as NoDishes;
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

class OrderTracking extends StatefulWidget {
  @override
  _OrderTrackingState createState() => _OrderTrackingState();
}

class _OrderTrackingState extends State<OrderTracking> {
  FirebaseUser user;
  bool _pagoTarjeta = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Seguimiento", style: textStylew500),
          elevation: 0.0,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: getWidget()
    );
  }

  getWidget() {
    if (user != null) {
      return StreamBuilder(
        stream: Firestore.instance
            .collection("orders")
            .where("uid", isEqualTo: user.uid)
            .where("finish", isEqualTo: false)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text("Error a lv");
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Loader.Loading();
            default:
              if (snapshot.data.documents.length > 0) {
                return new ListView(
                  children:
                      snapshot.data.documents.map((DocumentSnapshot document) {
                    Tracking tracking = Tracking.fromJson(document.data);
                    return new Container(
                      child: ExpansionTile(
                        initiallyExpanded: true,
                        title: Text(tracking.uid == user.uid ? "Mi Orden" : user.uid),
                        children: <Widget>[
                          countReady(tracking.items) ? createButton(tracking.items, context, document.documentID, _pagoTarjeta) : Container(),
                          countReady(tracking.items) ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text("Active para pagar con tarjeta:"),
                              Switch(
                                value: _pagoTarjeta,
                                onChanged: (value) {
                                  setState(() {
                                    _pagoTarjeta = value;
                                  });
                                },
                              ),
                              SizedBox(height: 30,),
                            ],
                          ) : Container(),
                          Container(
                            color: Colors.white,
                            child: Column(
                              children: 
                                builderList(tracking.items, context, document.documentID),
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                );
              }else{
                return NoDishes.NoDishes(msg: "No ha ordenado aún.",);
              }
          }
        },
      );
    } else {
      return Loader.Loading();
    }
  }

  void setUser() async {
    FirebaseUser userF = await getuser();
    if (userF != null) {
      if (mounted) {
        setState(() {
          user = userF;
        });
      }
    }
  }
}

List<Widget> builderList(List<Item> items, BuildContext context, String docId) {
  List<Widget> itemsWidgetList = <Widget>[];

  for (var i = 0; i < items.length; i++) {
    Item item = items[i];

    itemsWidgetList.add(Column(
      children: <Widget>[
        ListTile(
            subtitle: Text('  \$' + item.price.toStringAsFixed(2)),
            key: Key(UniqueKey().toString()),
            title: new Text(
              ' (' + item.quantity.toString() + ') ' + item.currentName,
              style: const TextStyle(fontSize: 15.0),
            ),
            trailing: new Container(
              width: 150.0,
              child: new Row(
                children: <Widget>[buildPhase(item, i, context, docId)],
              ),
            )),
        item.selectedComplements.length > 0
            ? new ExpansionTile(
                title: Text('Complementos ' + item.currentName),
                children: <Widget>[
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: buildComplemento(item.selectedComplements, i, context, docId, item.phase),
                  )
                ],
              )
            : new Text("")
      ],
    ));
  }

  return itemsWidgetList;
}

Widget buildPhase(Item item, int index, BuildContext context, String docId) {
  switch (item.phase) {
    case 0:
      return new Container(
        alignment: Alignment.centerLeft,
        width: 150,
        child: Column(
        children: <Widget>[
          GestureDetector(
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
              child: Row(
                children: <Widget>[
                  Text("En Espera",
                      style: defaultTargetPlatform == TargetPlatform.iOS
                          ? new TextStyle(
                              fontWeight: FontWeight.w100,
                              color:
                                  const Color.fromRGBO(153, 153, 153, 1.0))
                          : new TextStyle(
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(
                                  153, 153, 153, 1.0))),
                  FlatButton(
                    onPressed: () {
                      _createAlert(index, context, docId, 1);
                    },
                    child: Icon(
                      Icons.remove_circle,
                      color: Colors.red,
                    ),
                  )
                ],
              ),
            ))
          ],
        ),
      );
      break;
    case 1:
      return new Container(
        alignment: Alignment.centerLeft,
        width: 150,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
              child: Row(
                children: <Widget>[
                  Text("Preparando",
                      style: defaultTargetPlatform == TargetPlatform.iOS
                          ? new TextStyle(
                              fontWeight: FontWeight.w100,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))
                          : new TextStyle(
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))),
                  SizedBox(width: 10.0,),
                  Icon(
                    Icons.alarm,
                    color: Colors.yellow[600],
                  ),
                ],
              ),
            )
          ],
        ),
      );
      break;
    case 2:
      return new Container(
        alignment: Alignment.centerLeft,
        width: 150,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
              child: Row(
                children: <Widget>[
                  Text("Listo",
                      style: defaultTargetPlatform == TargetPlatform.iOS
                          ? new TextStyle(
                              fontWeight: FontWeight.w100,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))
                          : new TextStyle(
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))),
                  SizedBox(width: 10.0,),
                  Icon(
                    Icons.check,
                    color: Colors.green[600],
                  ),
                ],
              ),
            )
          ],
        ),
      );
      break;
    case 4:
      return new Container(
        alignment: Alignment.centerLeft,
        width: 150,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
              child: Row(
                children: <Widget>[
                  Text("Solicitando cancelación",
                      style: defaultTargetPlatform == TargetPlatform.iOS
                          ? new TextStyle(
                              fontWeight: FontWeight.w100,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))
                          : new TextStyle(
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(153, 153, 153, 1.0))),
                ],
              ),
            )
          ],
        ),
      );
    break;

    default:
      return new Text("Otro",
          style: defaultTargetPlatform == TargetPlatform.iOS
              ? new TextStyle(
                  fontWeight: FontWeight.w100,
                  color: const Color.fromRGBO(153, 153, 153, 1.0))
              : new TextStyle(
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(153, 153, 153, 1.0)));
      break;
  }
}

Future<void> _createAlert(int indexOrder, BuildContext context, String docId, int opt, [int indexComplemento]){
  switch (opt) {
    case 1:
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext c) {
          return AlertDialog(
            title: Text('¿Desea solicitar la cancelación de este platillo?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Aceptar'),
                textColor: Colors.blueAccent,
                onPressed: () {
                  //print("Posición: " + index.toString());
                  var dataToSend = {
                    'idOrder': docId,
                    'indexToCancel': indexOrder.toString()
                  };
                  requestUser("POST", '/orders/cancelDishes', 
                  dataToSend, context)
                  .then((response) {
                    var result = json.decode(response.body);
                    print(result);
                  }).catchError((onError) {
                    print(onError);
                  });
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text('Cancelar'),
                textColor: Colors.red,
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
      );
    break;
    case 2:
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext c) {
          return AlertDialog(
            title: Text('¿Desea cancelar el complemento de este platillo?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Aceptar'),
                textColor: Colors.blueAccent,
                onPressed: () {
                  var dataToSend = {
                    'idOrder': docId,
                    'indexItems': indexOrder.toString(),
                    'indexComplements': indexComplemento.toString()
                  };
                  requestUser("POST", '/orders/deleteComplements', 
                  dataToSend, context)
                  .then((response) {
                    var result = json.decode(response.body);
                    print(result);
                  }).catchError((onError) {
                    print(onError);
                  });
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text('Cancelar'),
                textColor: Colors.red,
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
      );
    break;
  }
}

List<Widget> buildComplemento(List<ComplementItem> complementos, int index, BuildContext context, String docId, int phase) {
  List<Widget> complementosWidgetList = <Widget>[];
  for (var i = 0; i < complementos.length; i++) {
    ComplementItem complemento = complementos[i];
    if(phase == 0){
      complementosWidgetList.add(new ListTile(
      title: new Text(
        complemento.nombre,
        style: const TextStyle(fontSize: 15.0),
      ),
      trailing: new Container(
          alignment: Alignment.centerLeft,
          width: 150.0,
          child: Row(
            children: <Widget>[
              new Text('\$${complemento.precio.toStringAsFixed(2)}'),
              new FlatButton(
                onPressed: () {
                  _createAlert(index, context, docId, 2, i);
                },
                child: Icon(
                  Icons.remove_circle,
                  color: Colors.red,
                ),
              )
            ],
          )),
      ));
    }else{
      complementosWidgetList.add(new ListTile(
      title: new Text(
        complemento.nombre,
        style: const TextStyle(fontSize: 15.0),
      ),
      trailing: new Container(
          alignment: Alignment.centerLeft,
          width: 150.0,
          child: Row(
            children: <Widget>[
              new Text('\$${complemento.precio.toStringAsFixed(2)}'),
            ],
          )),
      ));
    }
  }

  return complementosWidgetList;
}

bool countReady(List<Item> items){
  int count = 0;
  for(var i = 0; i < items.length; i++){
    if(items[i].phase == 2){
      count++;
    }
  }
  if(count == items.length){
    return true;
  }else{
    return false;
  }
}

Widget createButton(List<Item> items, BuildContext context, String docId, bool pagoT){
  final Size screenSize = MediaQuery.of(context).size;
  return Container(
    child: new InkWell(
      onTap: () {
        finishOrder(docId, context, pagoT);
      },
      child: new Container(
        child: const Text(
          'Solicitar cuenta',
          style: const TextStyle(
          color: Colors.white,
          fontSize: 14.0,
          fontWeight: FontWeight.bold),
        ),
        width: screenSize.width - 20,
        height: 45.0,
        alignment: FractionalOffset.center,
        decoration: new BoxDecoration(
          color: Colors.blueAccent,
          borderRadius:
              const BorderRadius.all(const Radius.circular(5.0)),
        ),
      ),
    ),
  );
}

finishOrder(String docId, BuildContext context, bool pagoT){
  showDialog<void>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext c) {
      return AlertDialog(
        title: Text('¿Desea solicitar su cuenta?'),
        content: pagoT ? Text("Paga con tarjeta seleccionado", style: TextStyle(fontSize: 14)) : Text("Pago con efectivo", style: TextStyle(fontSize: 14),),
        actions: <Widget>[
          FlatButton(
            child: Text('Aceptar'),
            textColor: Colors.blueAccent,
            onPressed: () {
              if(pagoT){
                Toast.show("Pago con tarjeta", context, duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
              }else{
                Toast.show("Pago sin tarjeta", context, duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
              }
            },
          ),
          FlatButton(
            child: Text('Cancelar'),
            textColor: Colors.red,
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      );
    }
  );
}