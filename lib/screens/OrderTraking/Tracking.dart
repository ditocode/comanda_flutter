class Tracking {
  bool finish;
  DateTime created;
  List<Item> items;
  String uid;

  Tracking({this.finish, this.created, this.items,this.uid});

  factory Tracking.fromJson(Map<String, dynamic> json) {
    List<Item> items = <Item>[];

    DateTime created =
        new DateTime.fromMillisecondsSinceEpoch(json['created'] * 1000);

    if(json['items'] != null){
      json['items'].map((option) {
        Item i = Item.fromJson(new Map<String, dynamic>.from(option));
        items.add(i);
      }).toList();
    }

    return Tracking(finish: json['finish'], created: created, items: items,uid: json['uid']);
  }
}

class Item {
  String currentName;
  String id;
  String instrunctions;
  int phase;
  double price;
  int quantity;
  List<ComplementItem> selectedComplements;

  Item(
      {this.currentName,
      this.id,
      this.instrunctions,
      this.phase,
      this.price,
      this.quantity,
      this.selectedComplements});

  factory Item.fromJson(Map<String, dynamic> json) {
    double price = 0.0;
    int phase = 0;
    int quantity = 0;
    List<ComplementItem> selectedComplement = <ComplementItem>[];

    if (json['price'] is int || json['price'] is String) {
      price = double.parse(json['price'].toString());
    } else {
      price = json['price'];
    }

    if (json['phase'] is String) {
      phase = int.parse(json['phase']);
    } else {
      phase = json['phase'];
    }

    if(json['quantity'] is String){
      quantity = int.parse(json['quantity']);
    }else{
      quantity = json['quantity'];
    }

    
      json['selectedComplements'].map((complement) {
        ComplementItem i =
            ComplementItem.fromJson(new Map<String, dynamic>.from(complement));
        selectedComplement.add(i);
      }).toList();
    

    return Item(
        currentName: json['currentName'],
        id: json['id'],
        phase: phase,
        price: price,
        quantity: quantity,
        selectedComplements: selectedComplement);
  }
}

class ComplementItem {
  String nombre;
  double precio;

  ComplementItem({this.nombre, this.precio});

  factory ComplementItem.fromJson(Map<String, dynamic> json) {
    double precio = 0.0;

    if (json['precio'] is int || json['precio'] is String) {
      precio = double.parse(json['precio'].toString());
    }

    return ComplementItem(nombre: json['nombre'], precio: precio);
  }
}
