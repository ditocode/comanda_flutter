import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';

import 'package:comandapp/screens/HomeWithTab/style.dart';

import 'package:comandapp/screens/Cart/index.dart' as Cart;
import 'package:comandapp/screens/Account/index.dart' as Account;
import 'package:comandapp/screens/Order/index.dart' as Order;
import 'package:comandapp/screens/OrderDetail/index.dart' as OrderDetail;
import 'package:comandapp/screens/Home/index.dart' as Home;
import 'package:comandapp/screens/Menu/index.dart' as Menu;
import 'package:comandapp/screens/OrderTraking/index.dart' as OrderTracking;
import 'package:scoped_model/scoped_model.dart';

class HomeWithTab extends StatefulWidget {
  var data;
  HomeWithTab({Key key, this.data}) : super(key: key);

  @override
  _HomeWithTabState createState() => new _HomeWithTabState();
}

TabController controller;

class _HomeWithTabState extends State<HomeWithTab>
    with TickerProviderStateMixin {
  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Container(
      child: ScopedModelDescendant<OrderModel>(
        builder: (context, child, model) => new Scaffold(
          primary: true,
          resizeToAvoidBottomPadding: false,
          backgroundColor: new Color.fromRGBO(246, 246, 246, 1.0),
          body: new TabBarView(
            children: <Widget>[
              new Menu.Menu(data: widget.data,model: model,),
              //new Home.Home(),
              new OrderDetail.OrderDetail(tabController: controller,),
              new OrderTracking.OrderTracking(),
            
              new Account.Account(),
            ],
            controller: controller,
          ),
          bottomNavigationBar: new Container(
            height: screenSize.height / 9,
            decoration: new BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: new TabBar(
              tabs: <Tab>[
                new Tab(
                  child: new Column(
                    children: <Widget>[
                      new ImageIcon(
                        restaurant,
                        size: 30.0,
                      ),
                      new Text(
                        "Platillos",
                        style: new TextStyle(fontSize: 11.0),
                      )
                    ],
                  ),
                ),
                new Tab(
                  child: new Column(
                    children: <Widget>[
                      new ImageIcon(
                        cart,
                        size: 30.0,
                      ),
                      new Text("Orden")
                     
                    ],
                  ),
                ),
                new Tab(
                  child: new Column(
                    children: <Widget>[
                      new ImageIcon(
                        order,
                        size: 30.0,
                      ),
                       new Text("Seguir", style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
                new Tab(
                  child: new Column(
                    children: <Widget>[
                      Icon(
                        Icons.settings,
                        size: 30.0,
                      ),
                       new Text(
                        "Config",
                        style: new TextStyle(fontSize: 11.0))
                    ],
                  ),
                ),
              ],
              indicatorColor: Colors.transparent,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.white30,
              controller: controller,
              labelStyle: new TextStyle(
                fontSize: 12.0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
