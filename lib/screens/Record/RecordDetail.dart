import 'dart:async';
import 'dart:convert';
import 'package:comandapp/components/cardMenuData.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:comandapp/screens/DishDetail/index.dart';
import 'package:comandapp/screens/Record/Record.dart';
import 'package:comandapp/tools/tool.dart';
import 'package:comandapp/tools/user.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

class RecordDetail extends StatelessWidget {
  TabController tabController;

  RecordDetail({this.tabController});

    @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final RecordModel order = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Detalles de la orden", style: textStylew500),
          elevation: 0.0,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: buildOrder(context, order, screenSize),
    );
  }
}

Widget buildOrder(BuildContext context, RecordModel order, Size screen){
  return Container(
    child: Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(8.0, 5.0, 0, 0),
              child: Text("Restaurante:", style: TextStyle(fontWeight: FontWeight.bold),),
              alignment: Alignment.centerLeft,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 5.0),
              height: screen.height * 0.05,
              alignment: Alignment.bottomLeft,
              child: Text(order.sucursal),
            ),
          ],
        ),
        SizedBox(
          height: 10.0,
          child: Center(
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
              height: 2.0,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(8.0, 5.0, 0, 5.0),
                child: Text("Platillo (Cantidad)", style: TextStyle(fontWeight: FontWeight.bold),),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(width: screen.width * 0.48,),
              Container(
                padding: EdgeInsets.fromLTRB(0, 5.0, 10.0, 0),
                child: Text("Precio", style: TextStyle(fontWeight: FontWeight.bold),),
                alignment: Alignment.centerLeft,
              ),
            ],
          ),
        ),
        Container(
          child: Column(
            children: buildDishes(context, order.items, screen),
          ),
        ),
        SizedBox(
          height: 10.0,
          child: Center(
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
              height: 2.0,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          height: screen.height * 0.06,
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(10.0, 5.0, 0, 0),
                alignment: Alignment.topLeft,
                child: Text("Total:", style: TextStyle(fontWeight: FontWeight.bold),),
              ),
              SizedBox(width: screen.width * 0.6,),
              Container(
                padding: EdgeInsets.fromLTRB(0, 5.0, 10.0, 0),
                alignment: Alignment.topRight,
                child: Text(order.getTotal()),
              ),
            ],
          ),
        )
      ],
    ),
  );
}

List<Widget> buildDishes(BuildContext contex, List<Item> dishes, Size screen){
  List<Widget> list = new List<Widget>();

  for(var i = 0; i < dishes.length; i++){
    String name = dishes[i].currentName;
    int quantity = dishes[i].quantity;
    String price = dishes[i].getTotalDish();

    list.add(
      Row(
        children: <Widget>[
          Container(
            width: screen.width * 0.8,
            padding: EdgeInsets.fromLTRB(8.0, 5.0, 0, 5.0),
            alignment: Alignment.centerLeft,
            child: Text("$name  ($quantity)"),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5.0, 5.0, 0, 5.0),
            alignment: Alignment.centerRight,
            child: Text(price),
          ),
        ],
      )
    );
  }

  return list;
}
