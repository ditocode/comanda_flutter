class RecordModel {
  bool finish;
  String created;
  List<Item> items;
  String uid;
  String sucursal;

  RecordModel({this.finish, this.created, this.items,this.uid,this.sucursal});

  factory RecordModel.fromJson(Map<String, dynamic> json) {
    List<Item> items = <Item>[];

    DateTime created = DateTime.fromMillisecondsSinceEpoch(json['created']);
    String date = created.day.toString()+"/"+created.month.toString()+"/"+created.year.toString();

    if(json['items'] != null){
      json['items'].map((option) {
        Item i = Item.fromJson(new Map<String, dynamic>.from(option));
        items.add(i);
      }).toList();
    }

    return RecordModel(finish: json['finish'], created: date, items: items,uid: json['uid'], sucursal: json['idSucursal']);
  }

  String getTotal() {
    double total = 0;

    items.forEach((item) {
      double totalByItem = item.price * item.quantity;
      double totalOptions = 0.0;
      item.selectedComplements.forEach((option) {
        totalOptions += option.precio;
      });

      total += totalByItem + (totalOptions * item.quantity);
    });

    return '\$${total.toStringAsFixed(2)} MXN';
  }
}

class Item {
  String currentName;
  String id;
  String instrunctions;
  int phase;
  double price;
  int quantity;
  List<ComplementItem> selectedComplements;

  Item(
      {this.currentName,
      this.id,
      this.instrunctions,
      this.phase,
      this.price,
      this.quantity,
      this.selectedComplements});

  factory Item.fromJson(Map<String, dynamic> json) {
    double price = 0.0;
    int phase = 0;
    int quantity = 0;
    List<ComplementItem> selectedComplement = <ComplementItem>[];

    if (json['price'] is int || json['price'] is String) {
      price = double.parse(json['price'].toString());
    } else {
      price = json['price'];
    }

    if (json['phase'] is String) {
      phase = int.parse(json['phase']);
    } else {
      phase = json['phase'];
    }

    if(json['quantity'] is String){
      quantity = int.parse(json['quantity']);
    }else{
      quantity = json['quantity'];
    }

    
      json['selectedComplements'].map((complement) {
        ComplementItem i =
            ComplementItem.fromJson(new Map<String, dynamic>.from(complement));
        selectedComplement.add(i);
      }).toList();
    

    return Item(
        currentName: json['currentName'],
        id: json['id'],
        phase: phase,
        price: price,
        quantity: quantity,
        selectedComplements: selectedComplement);
  }

    String getTotalDish() {
    double total = 0;

      double totalByItem = price * quantity;
      double totalOptions = 0.0;
      selectedComplements.forEach((option) {
        totalOptions += option.precio;
      });

      total += totalByItem + (totalOptions * quantity);

    return '\$${total.toStringAsFixed(2)}';
  }
}

class ComplementItem {
  String nombre;
  double precio;

  ComplementItem({this.nombre, this.precio});

  factory ComplementItem.fromJson(Map<String, dynamic> json) {
    double precio = 0.0;

    if (json['precio'] is int || json['precio'] is String) {
      precio = double.parse(json['precio'].toString());
    }

    return ComplementItem(nombre: json['nombre'], precio: precio);
  }
}
