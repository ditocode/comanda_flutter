import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:comandapp/screens/Cart/index.dart' as prefix0;
import 'package:comandapp/screens/OrderTraking/Tracking.dart';
import 'package:comandapp/screens/Record/Record.dart' as prefix1;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/screens/Commons/loading.dart' as Loader;
import 'package:comandapp/screens/Commons/noDishes.dart' as NoDishes;
import 'package:comandapp/tools/user.dart';

class Record extends StatefulWidget {
  @override
  _RecordState createState() => _RecordState();
}

class _RecordState extends State<Record> {
  FirebaseUser user;
  Firestore firestore = Firestore.instance;
  ScrollController _scrollController;

  @override
  void initState() {
    setUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Historial de ordenes", style: textStylew500),
          elevation: 0.0,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: getWidget()
    );
  }

  getWidget() {
    final Size screenSize = MediaQuery.of(context).size;
    if (user != null) {
      return StreamBuilder(
        stream: Firestore.instance
            .collection("orders")
            .where("uid", isEqualTo: user.uid)
            .where("finish", isEqualTo: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text("Error a lv");
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Loader.Loading();
            default:
              if (snapshot.data.documents.length > 0) {
                return new ListView(
                  controller: _scrollController,
                  children: snapshot.data.documents.map((DocumentSnapshot document) {
                    prefix1.RecordModel record = prefix1.RecordModel.fromJson(document.data);
                    return new Container(
                      child: Column(
                        children: builderList(record, context, screenSize),
                      ),
                    );
                  }).toList(),
                );
              }else{
                return NoDishes.NoDishes(msg: "No hay ninguna orden aún.",);
              }
          }
        },
      );
    } else {
      return Loader.Loading();
    }
  }

  void setUser() async {
    FirebaseUser userF = await getuser();
    if (userF != null) {
      if (mounted) {
        setState(() {
          user = userF;
        });
      }
    }
  }
}

List<Widget> builderList(prefix1.RecordModel items, BuildContext context, Size screen){
  SizedBox(height: screen.height * 0.01,);
  List<Widget> itemsWidgetList = <Widget>[];
  itemsWidgetList.add(Column(
    children: <Widget>[
      Card(
        elevation: 3.0,
        child: InkWell(
          splashColor: Colors.lightBlue,
          onTap: () {
            Navigator.pushNamed(context, '/RecordDetail', arguments: items);
          },
          child: Container(
            padding: EdgeInsets.all(10.0),
            width: screen.width - 20,
            height: screen.height * 0.18,
            child: Column(
              children: <Widget>[
                Container(
                  child: Text("${items.sucursal}"),
                  alignment: Alignment.topLeft,
                ),
                SizedBox(height: screen.height * 0.015,),
                Container(
                  child: Text("${items.getTotal()}", style: TextStyle(fontSize: 40)),
                  alignment: Alignment.center,
                ),
                SizedBox(height: screen.height * 0.01,),
                Container(
                  child: Text("${items.created}"),
                  alignment: Alignment.bottomRight,
                ),
              ],
            ),
          ),
        ),
      ),
    ],
  ));

  return itemsWidgetList;
}
