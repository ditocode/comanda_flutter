import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/components/cardMenuData.dart';
import 'package:flutter/foundation.dart';
import 'package:comandapp/screens/DishDetail/index.dart';

class ItemCardBuilder extends StatelessWidget {
  final List<ItemOrder> itemList;

  const ItemCardBuilder({this.itemList});

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return (new Column(
        children: itemList.map((ItemOrder homeCardData) {
      return GestureDetector(
          onTap: () {
            var route = new MaterialPageRoute(
              builder: (BuildContext context) => new DishDetail(
                dish: homeCardData,
              ),
            );

            Navigator.of(context).push(route);
          },
          child: new Card(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new SizedBox(
                  width: 7 * screenSize.width / 8.5,
                  child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          padding: const EdgeInsets.only(
                              top: 10.0, bottom: 10.0, left: 10.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 70.0,
                                height: 70.0,
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image: new Image.network(
                                            homeCardData.thumbail)
                                        .image,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: new BorderRadius.circular(5.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Flexible(
                          child: new Container(
                            padding: const EdgeInsets.all(10.0),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Padding(
                                  child: new Text(
                                    homeCardData.currentName,
                                    softWrap: true,
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .display1,
                                  ),
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                ),
                                new Text(
                                  homeCardData.description,
                                  style: defaultTargetPlatform ==
                                          TargetPlatform.iOS
                                      ? Theme.of(context)
                                          .primaryTextTheme
                                          .display2
                                      : Theme.of(context)
                                          .primaryTextTheme
                                          .display4,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]),
                ),
                new SizedBox(
                  width:
                      (screenSize.width) - (7 * screenSize.width / 8.5) - 8.0,
                  child: new Padding(
                    padding: const EdgeInsets.only(top: 10.0, right: 10.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Padding(
                          child: new Text('\$${homeCardData.price.toStringAsFixed(2)}',
                              softWrap: false,
                              style:
                                  Theme.of(context).primaryTextTheme.display3),
                          padding: const EdgeInsets.only(bottom: 10.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ));
    }).toList()));
  }
}
