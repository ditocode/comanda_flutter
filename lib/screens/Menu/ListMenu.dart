import 'dart:convert';

import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/tools/user.dart';
import 'package:comandapp/components/menuCard.dart';
import 'package:comandapp/components/menuCardData.dart';
import 'package:comandapp/components/cardMenuData.dart';

import 'package:comandapp/screens/Menu/ListCardMenu.dart';

class ListMenu extends StatefulWidget {
  var idMenu; //Se deja para hacer el seguimiento en firestore por si existe algun cambio
  var dishes;
  ListMenu({Key key, this.idMenu,this.dishes}) : super(key: key);
  @override
  _ListMenuState createState() => _ListMenuState();
}

class _ListMenuState extends State<ListMenu> {
  List<menuCardData> _menuCards = <menuCardData>[];
  List<ItemOrder> _cardMenu = <ItemOrder>[];
  var cargando = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState(); 
    makeCards(widget.dishes['dishes']);
  }

  @override
  Widget build(BuildContext context) {
    if (!cargando) {
      return new ListView(
        children: <Widget>[
          // new MenuCardBuilder(
          //   cards: _menuCards,
          // )
            new ItemCardBuilder(
            itemList: _cardMenu,
          )
        ],
      );
    } else {
      return new Column(
        children: <Widget>[new Text(widget.idMenu.toString())],
      );
     
    }
  }

  void makeCards(dishes) {
    List<menuCardData> menuCards = <menuCardData>[];
    List<ItemOrder> cardMenu = <ItemOrder>[];
    
    for (var i = 0; i < dishes.length; i++) {
      var data = dishes[i];
      menuCardData card = new menuCardData(
          image: data['img']['url'],
          dishName: data['nombre'],
          amount: data['precio'],
          veg: true,
          internet: true,
          quantity: 0);
      menuCards.add(card);
      List<Complemento> complementosList = <Complemento>[];

        data['complementos'].map((item){
          var complementos = new Complemento.fromJson(item);
          complementosList.add(complementos);
        }).toList();

    
    //print(complementosList);

      ItemOrder cardMenuData = new ItemOrder(
        thumbail: data['img']['url'],
        price: double.parse(data['precio']),
        currentName: data['nombre'],
        description: data['descripcion'],
        id: data['id'],
        complements: complementosList
      );
      cardMenu.add(cardMenuData);
    }

    setState(() {
      _menuCards = menuCards;
      _cardMenu = cardMenu;
      cargando = false;
    });
  }
}
