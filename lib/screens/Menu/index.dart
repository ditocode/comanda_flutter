import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/screens/Menu/ListMenu.dart';

class Menu extends StatefulWidget {
  var data;
  OrderModel model;

  //Construye las tabs

  Menu({Key key, this.data, this.model}) : super(key: key);

  @override
  _Menu createState() => new _Menu();
}

class _Menu extends State<Menu> with TickerProviderStateMixin {
  var existingOrder = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var tabs = getTabs(widget.data);
    var listMenu = getMenus(widget.data);
    final TabController controller =
        new TabController(length: tabs.length, vsync: this);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Carta',
          style: textStylew500,
        ),
        leading: returnLeading(),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        centerTitle: true,
        bottom: new TabBar(
          tabs: tabs,
          controller: controller,
          isScrollable: true,
          labelColor: Theme.of(context).primaryColor,
        ),
      ),
      body: new TabBarView(
        children: getMenus(widget.data),
        controller: controller,
      ),
    );
  }

  Widget returnLeading() {
    if (widget.model.orderId != "") {
      return new Text('');
    } else {
      return new IconButton(
          icon: new Icon(
            Icons.center_focus_weak,
            size: 30.0,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/ScanQR');
          });
    }
  }

  List<Widget> getTabs(data) {
    List<Tab> tabs = new List();
    for (var i = 0; i < data['menu'].length; i++) {
      var obj = data['menu'][i];
      tabs.add(Tab(
        text: obj['nombre'].toString(),
        key: Key(UniqueKey().toString()),
      ));
    }
    return tabs;
  }

  List<Widget> getMenus(data) {
    List<Widget> menus = new List();
    
    widget.model.setTable(data['idMesa'], data['mesa']);
    
    if (data['idOrder'] != "") {
      widget.model.setOrderId(data['idOrder']);
      if (mounted) {
        setState(() {
          existingOrder = data['idOrder'];
        });
      }
    }

    for (var i = 0; i < data['menu'].length; i++) {
      var obj = data['menu'][i];
      widget.model.addIdSucursal(data['menu'][i]['idSucursal']);
      menus.add(new ListMenu(
        idMenu: obj['id'],
        dishes: data['menu'][i]['dishes'],
      ));
    }
    return menus;
  }
}
