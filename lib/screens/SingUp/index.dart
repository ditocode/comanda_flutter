import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/tools/user.dart';
import 'package:comandapp/tools/tool.dart';
import 'package:toast/toast.dart';

class SingUp extends StatefulWidget {
  @override
  _SingUpState createState() => _SingUpState();
}

class _SingUpState extends State<SingUp> {
  String _email, _password, _nombre, _apellidos;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Registro", style: textStylew500),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        leading: IconButton(
          icon: new Icon(
            Icons.chevron_left,
            size: 40.0,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/Login');
          }
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Form(
            key: _formkey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  validator: (input) {
                    if (input.trim().isEmpty) {
                      return 'Ingresa un nombre';
                    }
                  },
                  onSaved: (input) {
                    _nombre = input.trim();
                  },
                  decoration: InputDecoration(labelText: 'Nombre:'),
                ),
                TextFormField(
                  validator: (input) {
                    if (input.trim().isEmpty) {
                      return 'Ingresa tus apellidos';
                    }
                  },
                  onSaved: (input) {
                    _apellidos = input.trim();
                  },
                  decoration: InputDecoration(labelText: 'Apellidos:'),
                ),
                TextFormField(
                  validator: (input) {
                    if (input.trim().isEmpty) {
                      return 'Ingresa un correo electrónico';
                    }
                  },
                  onSaved: (input) {
                    _email = input.trim();
                  },
                  decoration: InputDecoration(labelText: 'Correo Electrónico:'),
                ),
                TextFormField(
                  validator: (input) {
                    if (input.trim().isEmpty) {
                      return 'Ingresa una contraseña';
                    }
                  },
                  onSaved: (input) {
                    _password = input.trim();
                  },
                  decoration: InputDecoration(labelText: 'Contraseña'),
                  obscureText: true,
                ),
                new Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: new InkWell(
                    onTap: singUp,
                    child: new Container(
                      child: new Text(
                        "Enviar",
                        style: const TextStyle(
                            color: Colors.white, fontSize: 14.0),
                      ),
                      height: 45.0,
                      alignment: FractionalOffset.center,
                      decoration: new BoxDecoration(
                        color: new Color.fromRGBO(33, 127, 255, 20.0),
                        borderRadius:
                            const BorderRadius.all(const Radius.circular(5.0)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> singUp() async {
    final formState = _formkey.currentState;

    if (formState.validate()) {
      formState.save();
      var data = {
        'email': _email,
        'password': _password,
        'name': _nombre,
        'secondName': _apellidos
      };
      var response = await requestUser(
          "POST", "/usuariosApp/createUserApp", data, context);
      if (response != null && response.statusCode == 200) {
        var resultRequest = json.decode(response.body);

        showToast(resultRequest['msg'], context, Toast.LENGTH_LONG,
            Toast.BOTTOM, Colors.green[400]);
        Navigator.of(context).pushReplacementNamed('/Login');
      } else if (response != null) {
         var resultRequest = json.decode(response.body);
         showToast(resultRequest['msg'], context, Toast.LENGTH_LONG,
            Toast.BOTTOM, Colors.red[400]);
      } else {
        showToast('Ocurrio un error inesperado intente de nuevo.', context, Toast.LENGTH_LONG,
            Toast.BOTTOM, Colors.red[400]);
      }
    }
  }
}
