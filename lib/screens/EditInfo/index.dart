import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/tools/user.dart';

class EditInfo extends StatefulWidget {
  @override
  _EditInfo createState() => _EditInfo();
}

class _EditInfo extends State<EditInfo> {
  FirebaseUser _user;
  String _nombre, _password1, _password2;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void setUser() async {
    FirebaseUser userF = await getuser();
    if (userF != null) {
      if (mounted) {
        setState(() {
          _user = userF;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    setUser();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      resizeToAvoidBottomPadding: true,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: new AppBar(
        centerTitle: true,
        title: new Text('Información', style: textStylew500),
        elevation: 0.0,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Center(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: screenSize.height*0.05,),
                  Container(
                    child: Icon(
                      Icons.account_circle,
                      size: screenSize.width*0.40,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      maxLength: 150,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      initialValue: _user.displayName,
                      validator: (input) {
                        if(input.trim().isEmpty){
                          return "Ingrese un nombre";
                        }
                      },
                      onSaved: (input) {
                        _nombre = input.trim();
                      },
                      decoration: InputDecoration(
                        labelText: 'Nombre',
                        icon: Icon(Icons.person),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      maxLength: 50,
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      validator: (input) {
                        if(_password2.isNotEmpty && input.isEmpty){
                          return "Escriba una contraseña";
                        }
                        if(input.trim().isNotEmpty && input.trim().length < 8){
                          return "La contraseña debe ser mínimo de 8 caracteres";
                        }
                      },
                      onSaved: (input) {
                        _password1 = input.trim();
                      },
                      decoration: InputDecoration(
                        labelText: 'Contraseña',
                        icon: Icon(Icons.lock),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      maxLength: 50,
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      validator: (input) {
                        if(_password1.isNotEmpty && input.isEmpty){
                          return "Confirme su contraseña";
                        }
                        if(input.trim().isNotEmpty && input != _password1){
                          return "Las contraseñas no coinciden";
                        }
                      },
                      onSaved: (input) {
                        _password2 = input.trim();
                      },
                      decoration: InputDecoration(
                        labelText: 'Confirmar contraseña',
                        icon: Icon(Icons.lock_outline),
                      ),
                    ),
                  ),
                  SizedBox(height: screenSize.height*0.05),
                  RaisedButton(
                    elevation: 0.0,
                    padding: EdgeInsets.only(top: 0.0),
                    child: Container(
                      height: 45.0,
                      alignment: FractionalOffset.center,
                      decoration: new BoxDecoration(
                        color: new Color.fromRGBO(33, 127, 255, 20.0),
                        borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
                      ),
                      child: Text("Actualizar datos",style: TextStyle(color: Colors.white, fontSize: 14.0),)
                    ),
                    onPressed: () {
                      updateInfo();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  updateInfo(){
    final state = _formKey.currentState;
    state.save();

    if(state.validate()){
      print("Todo bien");
      print(_user.uid);
      print(_nombre);
      print(_password1);
      print(_password2);
    }else{
      print("Algo anda mal");
      print(_nombre);
      print(_password1);
      print(_password2);
    }
  }
}