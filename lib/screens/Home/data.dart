 class HomeCardData {
  String thumbnail;
  String name;
  String timing;
  String quisine;
  String price;
  String offer1;
  String offer2;

  HomeCardData(
      {this.name,
      this.offer1,
      this.offer2,
      this.price,
      this.quisine,
      this.thumbnail,
      this.timing});
}

class DataListBuilder {
  List<HomeCardData> cardList = new List<HomeCardData>();

  HomeCardData card1 = new HomeCardData(
      thumbnail: "assets/home-page/rajdhani.jpg",
      name: "RajDhani",
      timing: "12PM to 10PM",
      quisine: "Rajasthani Lunch",
      price: "Rs. 600/head",
      offer1: "20% off on Dinner",
      offer2: "Tuesday Lunch @225");

  HomeCardData card2 = new HomeCardData(
      thumbnail: "assets/home-page/kamat-swaad.jpg",
      name: "Kamat Swaad",
      timing: "12PM to 4PM",
      quisine: "Festive Kannada Food",
      price: "Rs. 200/head",
      offer1: "15% off for Lunch",
      offer2: "Unlimited Rotti");
  HomeCardData card3 = new HomeCardData(
      thumbnail: "assets/home-page/chhattisgarhi-thali.jpg",
      name: "Chhattisgarhi Thali",
      timing: "1PM to 10PM",
      quisine: "Chhattisgarhi Lunch",
      price: "Rs. 250/head",
      offer1: "10% off for Lunch",
      offer2: "Unlimited Desserts");
  HomeCardData card4 = new HomeCardData(
      thumbnail: "assets/home-page/kathiawadi-thali.jpg",
      name: "Kathiawadi Thali",
      timing: "11PM to 11PM",
      quisine: "Gujarati dishes",
      price: "Rs. 300/head",
      offer1: "12% off for Lunch",
      offer2: "Unlimited Dhokla");
  HomeCardData card5 = new HomeCardData(
      thumbnail: "assets/home-page/annapoorni.jpg",
      name: "AnnaPoorni",
      timing: "12PM to 9PM",
      quisine: "Authentic Andhra Food",
      price: "Rs 220/head",
      offer1: "16% off for Dinner",
      offer2: "Unlimited Rice");
  DataListBuilder() {
    cardList.add(card1);
    cardList.add(card2);
    cardList.add(card3);
    cardList.add(card4);
    cardList.add(card5);
  }
}
