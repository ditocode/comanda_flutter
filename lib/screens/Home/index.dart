import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:comandapp/components/searchBar.dart';
import 'package:comandapp/screens/Home/style.dart';
import 'package:comandapp/components/homeBanner.dart';
import 'package:comandapp/components/customGridTile.dart';
import 'package:comandapp/components/itemCard.dart';
import 'package:comandapp/screens/Home/data.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  final DataListBuilder homeCardDataList = new DataListBuilder();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: new PreferredSize(
        preferredSize: new Size(screenSize.width, 55.0),
        child: new AppBar(
          elevation: 5.0,
          leading: new Icon(
            Icons.location_on,
            color: Theme.of(context).primaryColor,
          ),
          title: new Text(
            "Current address appears here",
            style: new TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.w500,
                fontSize: 16.0),
          ),
//          new TextFormField(
//            initialValue: "Location appears here",
//            style: new TextStyle(
//              color: Theme.of(context).primaryColor,
//              fontWeight: FontWeight.w500,
//            ),
//            keyboardType: TextInputType.text,
//            decoration: const InputDecoration(border: InputBorder.none),
//          ),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(
                  Icons.favorite,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed("/Favourites");
                })
          ],
          brightness: Theme.of(context).primaryColorBrightness,
          backgroundColor: Colors.white,
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new SearchBarBuilder(),
          new Container(
              margin: const EdgeInsets.only(top: 10.0),
              child: new HomeBannerBuilder()),
          new Container(
            padding: const EdgeInsets.only(top: 12.0, left: 7.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Flexible(
                  child: new CustomGrid(
                    image: indianFoodImg,
                    text: "Indian Food",
                    icon: namaste,
                    height: 150.0,
                    width: (screenSize.width - 30) / 2,
                    route: "/IndianFoodMenu",
                  ),
                ),
                new Flexible(
                  child: new CustomGrid(
                    image: foodCarnivalImg,
                    text: "Food Carnival",
                    icon: foodCarnivalLogo,
                    width: (screenSize.width - 30) / 2,
                    height: 150.0,
                    route: "/WesternFoodMenu",
                  ),
                ),
              ],
            ),
          ),
          new ItemCardBuilder(
            itemList: homeCardDataList.cardList,
          ),
        ],
      ),
    );
  }
}
