import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:comandapp/theme/style.dart';
import 'package:toast/toast.dart';
import 'package:comandapp/tools/tool.dart';
import 'package:comandapp/screens/HomeWithTab/index.dart';
import 'package:comandapp/tools/user.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QRScan(),
    ));

class QRScan extends StatefulWidget {
  @override
  QRScanState createState() {
    return new QRScanState();
  }
}

class QRScanState extends State<QRScan> {
  String result = "Ingresa o Escanea el Código situado en tu mesa.";
  bool chargin = false;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String _token = "";

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      print(qrResult);

      getMenu(
          context: context,
          url: '/menu/getMenuByIdQRApp',
          data: {'id': qrResult});
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Ingresa o Escanea el Código situado en tu mesa.";
        });
      } else {
        setState(() {
          result = "Ingresa o Escanea el Código situado en tu mesa.";
        });
      }
    } on FormatException {
      setState(() {
        result = "Ingresa o Escanea el Código situado en tu mesa.";
      });
    } catch (ex) {
      setState(() {
        result = "Ingresa o Escanea el Código situado en tu mesa.";
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getExistingOrder(context: context, url: '/orders/getOrder', data: {});
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: new Text('Escanea el QR de tú mesa', style: textStylew500),
        centerTitle: true,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: Center(
          child: new Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                children: <Widget>[
                  Text(
                    result,
                    style: new TextStyle(
                        fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  Form(
                    key: _formkey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          validator: (input) {
                            if (input.isEmpty) {
                              return 'Ingresa el código de tu mesa.';
                            }
                          },
                          onSaved: (input) {
                            _token = input;
                          },
                          initialValue: "BQVGDKZ6R",
                          decoration: InputDecoration(labelText: 'Código'),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: new InkWell(
                            onTap: getMenuByID,
                            child: new Container(
                              child: new Text(
                                defaultTargetPlatform == TargetPlatform.iOS
                                    ? "Enviar"
                                    : "ENVIAR",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 14.0),
                              ),
                              width: screenSize.width - 80,
                              height: 45.0,
                              alignment: FractionalOffset.center,
                              decoration: new BoxDecoration(
                                color: new Color.fromRGBO(33, 127, 255, 20.0),
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ))),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.center_focus_weak),
        label: Text("Scanear"),
        onPressed: _scanQR,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<void> getMenuByID() async {
    final formState = _formkey.currentState;
    if (formState.validate()) {
      formState.save();
      print('POR ID QR');
      getMenu(
          context: context,
          url: '/menu/getMenuByIdQRApp',
          data: {'id': _token});
    }
  }

  Future<void> getMenu({context, data, url, existingOrder}) async {
    var response = await requestUser("POST", url, data, context);

    if (response != null) {
      var resultRequest = json.decode(response.body);

      if (response.statusCode == 200) {
        if (existingOrder != null) {
          resultRequest['idOrder'] = existingOrder['idOrder'];
          resultRequest['idMesa'] = existingOrder['idTable'];
          resultRequest['mesa'] = existingOrder['tDescripcion'];
        } else {
          resultRequest['idOrder'] = "";
        }

        //Navigator.of(context).pushReplacementNamed('/HomeWithTab',arguments: result);
        var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
              new HomeWithTab(data: resultRequest),
        );

        Navigator.of(context).pushReplacement(route);
      } else {
        if (resultRequest['msg'] != "") {
          showToast(resultRequest['msg'], context, Toast.LENGTH_LONG,
              Toast.CENTER, Colors.red[400]);
        } else {
          showToast("No se logro leer el Código QR Intente de Nuevo.", context,
              Toast.LENGTH_LONG, Toast.CENTER, Colors.red[400]);
        }
      }
    } else {
      //showToast("No se logro leer el Código QR Intente de Nuevo.", context,
          //Toast.LENGTH_LONG, Toast.CENTER, Colors.red[400]);
    }
  }

  Future<void> getExistingOrder({context, data, url}) async {
    var response = await requestUser("POST", url, data, context);

    if (response != null && response.statusCode == 200) {
      var resultRequest = json.decode(response.body);
      var idSucursal = resultRequest['idSucursal'];
      if (idSucursal != null) {
        getMenu(
            context: context,
            url: '/menu/getMenyByIdSucursal',
            data: {'idSucursal': idSucursal},
            existingOrder: resultRequest);
      }
    } else {}
  }
}
