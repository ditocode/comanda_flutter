import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/src/foundation/platform.dart';
import 'package:toast/toast.dart';
import 'package:comandapp/tools/tool.dart';
import 'package:comandapp/theme/style.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String _email, _password;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        primary: true,
        appBar: AppBar(
          title: new Text('Login', style: textStylew500),
          centerTitle: true,
          backgroundColor: Theme.of(context).secondaryHeaderColor,
        ),
        body: new Padding(
          padding: const EdgeInsets.all(30.0),
          child: new Center(
            child: Form(
              key: _formkey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Ingresa un correo electrónico';
                      }
                    },
                    onSaved: (input) {
                      _email = input;
                    },
                    initialValue: "prueba@prueba.com",
                    decoration:
                        InputDecoration(labelText: 'Correo Electrónico'),
                  ),
                  TextFormField(
                    initialValue: "prueba1234",
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Ingresa una contraseña';
                      }
                    },
                    onSaved: (input) {
                      _password = input;
                    },
                    decoration: InputDecoration(labelText: 'Contraseña'),
                    obscureText: true,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: new InkWell(
                      onTap: signIn,
                      child: new Container(
                        child: new Text(
                          defaultTargetPlatform == TargetPlatform.iOS
                              ? "Ingresar"
                              : "INGRESAR",
                          style: const TextStyle(
                              color: Colors.white, fontSize: 14.0),
                        ),
                        width: screenSize.width - 80,
                        height: 45.0,
                        alignment: FractionalOffset.center,
                        decoration: new BoxDecoration(
                          color: new Color.fromRGBO(33, 127, 255, 20.0),
                          borderRadius: const BorderRadius.all(
                              const Radius.circular(5.0)),
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: new InkWell(
                      onTap: signUp,
                      child: new Container(
                        child: new Text( "¿Nuevo? Registrate",
                          style: const TextStyle(
                              color: Colors.white, fontSize: 14.0),
                        ),
                        width: screenSize.width - 80,
                        height: 45.0,
                        alignment: FractionalOffset.center,
                        decoration: new BoxDecoration(
                          color: new Color.fromRGBO(33, 127, 255, 20.0),
                          borderRadius: const BorderRadius.all(
                              const Radius.circular(5.0)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void signUp(){
    Navigator.of(context).pushReplacementNamed('/SingUp');
  }

  Future<void> signIn() async {
    //Validate Fields
    final formState = _formkey.currentState;

    if (formState.validate()) {
      //Login Firebase
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: _email, password: _password);
        Navigator.of(context).pushReplacementNamed('/splashScreen');
      } catch (e) {
        print(e.message);
        showToast(e.message, context, Toast.LENGTH_LONG, Toast.CENTER,
            Colors.red[400]);
      }
    }
  }
}
