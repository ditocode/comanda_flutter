import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/screens/Favourites/style.dart';
import 'package:comandapp/theme/style.dart';

class Order extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.chevron_left,
              size: 40.0,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed("/HomeWithTab");
            }),
        centerTitle: true,
        title: new Text(
          "Order",
          style: textStylew500,
        ),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      backgroundColor: Colors.white12,
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: const EdgeInsets.only(top: 70.0, bottom: 25.0),
              child: new CircleAvatar(
                child: new Image(image: favouritesImg),
                backgroundColor: Colors.white,
                radius: 100.0,
              ),
            ),
            new Text(
              "Please login to view your orders",
              textAlign: TextAlign.center,
              softWrap: true,
              style: new TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                  letterSpacing: -0.1),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: new InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/PhoneNumber");
                },
                child: new Container(
                  child: new Text(
                    defaultTargetPlatform == TargetPlatform.android
                        ? "LOGIN"
                        : "Login",
                    style: const TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                  width: screenSize.width - 80,
                  height: 45.0,
                  alignment: FractionalOffset.center,
                  decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(5.0)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
