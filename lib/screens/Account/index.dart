import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:comandapp/models/orderModel.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:comandapp/theme/style.dart';
import 'package:comandapp/tools/user.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  FirebaseUser user;

  void setUser() async {
    FirebaseUser userF = await getuser();
    if (userF != null) {
      if (mounted) {
        setState(() {
          user = userF;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    setUser();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: new AppBar(
        centerTitle: true,
        title: new Text('Preferencias', style: textStylew500),
        elevation: 0.0,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: Container(
        child: ScopedModelDescendant<OrderModel>(
          builder: (context, child, model) {
            return new Column(
              children: <Widget>[
                SizedBox(height: screenSize.height*0.10,),
                InkWell(
                  child: CustomCardAccount(
                    icon: Icons.person,
                    text: 'Información',
                    trailingIcon: Icons.arrow_forward_ios,
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/EditInfo');
                  },
                ),
                SizedBox(height: 10.0,),
                InkWell(
                  child: CustomCardAccount(
                    icon: Icons.list,
                    text: 'Mis ordenes',
                    trailingIcon: Icons.arrow_forward_ios,
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/Record');
                  },
                ),
                SizedBox(height: screenSize.height*0.30,),
                Container(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: new InkWell(
                    onTap: () {
                      signOut(context,model);
                    },
                    child: new Container(
                      child: const Text(
                        'Cerrar Sesión',
                        style: const TextStyle(
                            color: Colors.black54,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                      width: screenSize.width - 20,
                      height: 45.0,
                      alignment: FractionalOffset.center,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            const BorderRadius.all(const Radius.circular(5.0)),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class CustomCardAccount extends StatelessWidget {
  final IconData icon;
  final String text;
  final IconData trailingIcon;

  const CustomCardAccount({this.icon, this.text, this.trailingIcon});

  @override
  Widget build(BuildContext context) {
    return new Container(
//      color: Colors.white,
      decoration: new BoxDecoration(
          color: Colors.white,
          border: new Border(
              bottom: const BorderSide(
//              width: 0.3,
            color: Colors.black26,
          ))),
      child: new Container(
        margin: const EdgeInsets.only(left: 10.0),
        child: new ListTile(
          title: new Row(
            children: <Widget>[
              new Icon(
                icon,
                color: const Color.fromRGBO(153, 153, 153, 1.0),
              ),
              new Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: new Text(
                    text,
                    style: new TextStyle(
                        color: const Color.fromRGBO(68, 68, 68, 1.0),
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500),
                  )),
            ],
          ),
          trailing: new Icon(
            trailingIcon,
            size: 20.0,
          ),
        ),
      ),
    );
  }
}
