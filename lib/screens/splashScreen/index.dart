import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  SplashScreenstate createState() => new SplashScreenstate();
}

class SplashScreenstate extends State<SplashScreen> {
  Duration five;
  Timer t2;
  String routeName;

  @override
  void initState() {

    super.initState();
    five = const Duration(seconds: 5);
    t2 = new Timer(five, () {

      routeName = "/ScanQR";
      navigate(context, routeName);
    });
  }

  @override
  void dispose() {
    super.dispose();
    t2.cancel();
  }

  void navigate(BuildContext context, String routename) {
    if (routeName != null) {
      Navigator.of(context).pushReplacementNamed(routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return new Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
              image: new DecorationImage(
            image: const ExactAssetImage('assets/splashscreen.png'),
            fit: BoxFit.cover,
          )),
          child: new Container()),
    );
  }
}
