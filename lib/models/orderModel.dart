import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class OrderModel extends Model {
  List<ItemOrder> _items = [];
  List<ItemOrder> get items => _items;
  String _orderId = "";
  String get orderId => _orderId;
  String _idSucursal = "";
  String get sucursal => _idSucursal;
  Table _table;
  Table get table => _table;

  void setTable(String _idTable, String _description) {
    
    _table = new Table(idTable: _idTable, description: _description);
    
  }
  
  void setOrderId(String orderID) {
    if (orderID != _orderId) {
      _orderId = orderID;
      notifyListeners();
    }
  }

  void addIdSucursal(String idSucursal) {
    _idSucursal = idSucursal;
  }

  void cleanItems(context, TabController tabController) {
    _items = [];
    notifyListeners();
    Navigator.pop(context);
    tabController.animateTo(2);
  }

  void addItem(ItemOrder item, context) {
    _items.add(item);
    Navigator.pop(context);
  }

  void deleteItem(ItemOrder item) {
    _items.remove(item);
    notifyListeners();
  }

  void updateItem(index, ItemOrder item) {
    _items[index] = item;
    notifyListeners();
  }

  void updateItemNav(index, ItemOrder item, context) {
    _items[index] = item;
    notifyListeners();
    Navigator.pop(context);
  }

  String getTotal() {
    double total = 0;

    _items.forEach((item) {
      double totalByItem = item.price * item.quantity;
      double totalOptions = 0.0;
      item.selectedComplements.forEach((option) {
        totalOptions += option.precio;
      });

      total += totalByItem + (totalOptions * item.quantity);
    });

    return '\$${total.toStringAsFixed(2)} MXN';
  }
}

class ItemOrder {
  double price;
  String description;
  String currentName;
  String instructions = "";
  List<Complemento> complements = [];
  List<ComplementoOption> selectedComplements = [];
  String id;
  int quantity;
  int phase;
  String thumbail;

  ItemOrder(
      {this.price,
      this.currentName,
      this.id,
      this.quantity,
      this.phase,
      this.complements,
      this.instructions,
      this.description,
      this.thumbail,
      this.selectedComplements});

  Map<String, dynamic> toJsonAttr() => {
        'price': price,
        'description': description,
        'currentName': currentName,
        'instructions': instructions,
        'selectedComplements':
            selectedComplements.map((e) => e.toJsonAttr()).toList(),
        'id': id,
        'quantity': quantity,
        'phase': phase,
      };
  
  Map<String, dynamic> objToSend() => {
        'price': price,
        'currentName': currentName,
        'instructions': instructions,
        'selectedComplements':
            selectedComplements.map((e) => e.toJsonAttr()).toList(),
        'id': id,
        'quantity': quantity,
        'phase': phase
      };
  
  static List encondeToJson(List<ItemOrder> list) {
    List jsonList = List();
    list.map((item) => jsonList.add(item.objToSend())).toList();
    return jsonList;
  }
}

class Complemento {
  String question;
  bool optional = false;
  int max = 0;
  List<ComplementoOption> options;

  Complemento({this.question, this.max, this.optional, this.options});

  factory Complemento.fromJson(Map<String, dynamic> parsedJson) {
    List<ComplementoOption> complementosOptions = <ComplementoOption>[];

    parsedJson['options'].map((option) {
      var opt = new ComplementoOption.fromJson(option);
      complementosOptions.add(opt);
    }).toList();

    var maxSeleccion = 0;
    if (parsedJson['maxSeleccion'] is int) {
      maxSeleccion = parsedJson['maxSeleccion'];
    } else {
      maxSeleccion = int.parse(parsedJson['maxSeleccion']);
    }

    return Complemento(
        question: parsedJson['question'],
        optional: parsedJson['optional'],
        max: maxSeleccion,
        options: complementosOptions);
  }
}

class ComplementoOption {
  double precio;
  bool agotado;
  String nombre;
  int max;

  ComplementoOption({this.precio, this.agotado, this.nombre, this.max});

  Map<String, dynamic> toJsonAttr() => {
    'precio': precio,
    'nombre': nombre,
  };

  factory ComplementoOption.fromJson(Map<String, dynamic> parsedJson) {
    double precioParser = 0.0;

    if (parsedJson['precio'] is double) {
      precioParser = parsedJson['precio'];
    } else {
      if (parsedJson['precio'] != "") {
        precioParser = double.parse(parsedJson['precio']);
      }
    }

    return ComplementoOption(
        agotado: parsedJson['agotado'],
        precio: precioParser,
        nombre: parsedJson['nombre']
    );
  }
}

class Table {
  String idTable;
  String get getIdTable => idTable;
  String description;

  Table({this.idTable, this.description});

  Map<String, dynamic> toJsonAttr() => {
        'idTable': idTable,
        'description': description,
      };
}
