import 'package:flutter/material.dart';
import 'package:comandapp/screens/Home/style.dart';

class HomeBannerBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new SizedBox(
      height: screenSize.height / 4,
      child: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              image: fullWidthImg,
            ),
          ),
          new Container(
            height: screenSize.height / 4,
            width: screenSize.width,
            decoration: new BoxDecoration(
              color: Colors.black54,
            ),
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    'Refer your Friends',
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                  new Text(
                    'Earn Rs 200 off',
                    style: new TextStyle(
//                              fontSize: 20.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.white,
                    ),
                  ),
                  new Text(
                    'from selected restaurants',
                    style: new TextStyle(
//                              fontSize: 20.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.white,
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
