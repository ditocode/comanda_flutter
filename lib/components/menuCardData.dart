class menuCardData {
  String image;
  String dishName;
  String amount;
  bool internet=false;
  bool veg;
  int quantity;

  menuCardData(
      {this.amount, this.quantity, this.image, this.dishName, this.veg,this.internet});
}
