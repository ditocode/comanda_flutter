 class CardMenuData {
  String thumbnail;
  String name;
  String instructions;
  String description;
  String price;
  String id;

  CardMenuData(
      {this.name,
      this.description,
      this.price,
      this.thumbnail,
      this.id,
      this.instructions
      
      });
}